'use strict';

angular.module('currentVersionData', [])
  .value('CURRENT_NG_VERSION', {
  "raw": "v1.6.6",
  "major": 1,
  "minor": 6,
  "patch": 7,
  "prerelease": [
    "build",
    "5471"
  ],
  "build": "sha.6eb15cb",
  "version": "1.6.7-build.5471",
  "codeName": "snapshot",
  "isSnapshot": true,
  "full": "1.6.7-build.5471+sha.6eb15cb",
  "branch": "master",
  "cdn": {
    "raw": "v1.6.6",
    "major": 1,
    "minor": 6,
    "patch": 6,
    "prerelease": [],
    "build": [],
    "version": "1.6.6",
    "docsUrl": "http://code.angularjs.org/1.6.6/docs"
  }
});
